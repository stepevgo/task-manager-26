package ru.t1.stepanishchev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.repository.IRepository;
import ru.t1.stepanishchev.tm.enumerated.ProjectSort;
import ru.t1.stepanishchev.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> models = new ArrayList<>();

    @Override
    @Nullable
    public M add(@Nullable final M model) {
        models.add(model);
        return model;
    }

    @Override
    @NotNull
    public List<M> findAll() {
        return models;
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final ProjectSort sort) {
        if (sort == null) return findAll();
        return findAll(sort);
    }


    @Override
    @Nullable
    public M findOneById(@Nullable final String id) {
        return models
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public M findOneByIndex(@Nullable final Integer index) {
        return models.get(index);
    }

    @Override
    @Nullable
    public M removeOne(@Nullable final M model) {
        models.remove(model);
        return model;
    }

    @Override
    @Nullable
    public M removeOneById(@Nullable final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    @Nullable
    public M removeOneByIndex(@Nullable final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public void removeAll() {
        models.clear();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        models.removeAll(collection);
    }

    @Override
    @NotNull
    public Integer getSize() {
        return models.size();
    }

}