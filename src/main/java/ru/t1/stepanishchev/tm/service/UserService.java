package ru.t1.stepanishchev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.repository.IProjectRepository;
import ru.t1.stepanishchev.tm.api.repository.ITaskRepository;
import ru.t1.stepanishchev.tm.api.repository.IUserRepository;
import ru.t1.stepanishchev.tm.api.service.IPropertyService;
import ru.t1.stepanishchev.tm.api.service.IUserService;
import ru.t1.stepanishchev.tm.enumerated.Role;
import ru.t1.stepanishchev.tm.exception.entity.UserNotFoundException;
import ru.t1.stepanishchev.tm.exception.field.EmailEmptyException;
import ru.t1.stepanishchev.tm.exception.field.LoginEmptyException;
import ru.t1.stepanishchev.tm.exception.field.PasswordEmptyException;
import ru.t1.stepanishchev.tm.exception.user.ExistsEmailException;
import ru.t1.stepanishchev.tm.exception.user.ExistsLoginException;
import ru.t1.stepanishchev.tm.exception.user.RoleEmptyException;
import ru.t1.stepanishchev.tm.model.User;
import ru.t1.stepanishchev.tm.util.HashUtil;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ITaskRepository taskRepository;

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserRepository userRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IProjectRepository projectRepository
    ) {
        super(userRepository);
        this.propertyService = propertyService;
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (email == null || email.isEmpty()) throw new ExistsEmailException();
        @Nullable final User user = create(login, password);
        if (user != null) user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (role == null) throw new RoleEmptyException();
        @Nullable final User user = create(login, password);
        if (user == null) throw new UserNotFoundException();
        user.setRole(role);
        return user;
    }

    @Override
    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Override
    @Nullable
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Override
    @Nullable
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        return this.removeOne(user);
    }

    @Override
    @Nullable
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        return this.removeOne(user);
    }

    @Override
    @Nullable
    public User removeOne(@Nullable final User model) {
        if (model == null) return null;
        @Nullable final User user = super.removeOne(model);
        if (user == null) return null;
        @Nullable final String userId = user.getId();
        taskRepository.removeAll(userId);
        projectRepository.removeAll(userId);
        return user;
    }

    @Override
    @NotNull
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @Override
    @NotNull
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.isEmailExist(email);
    }

    @Override
    public void lockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        user.setLocked(false);
    }

}